# to convert python to json
import json
# a=(1,3,5,6)
# print(type(a))
# print(a)

# js=json.dumps(a)
# print(type(js))
# print(js)

student={
    'name':'Rajvir',
    'r_no':1,
    'marks':88.4,
    'subjects':('Python','Django'),
    'hobbies':['reading','playing','travelling'],
    'registered':True,
    're-appear':None,
    'absent':False,
}
print(student)
# to_json=json.dumps(student)
# formatting the result
to_json=json.dumps(student,separators=('@','-'),indent=5)
print(to_json)
print(student['name'])
# print(to_json[0])

# to check module location
